# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    "*** YOUR CODE HERE ***"
    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    # Algoritmo: Búsqueda en grafo por profundidad
    # Se inicializa una pila para la frontera y una lista para los nodos explorados.
    # Utiliza una pila (util.Stack()) para la frontera. Esto significa que el último nodo agregado 
    # a la frontera será el primero en ser explorado. Es decir, se explora tan profundamente como 
    # sea posible antes de retroceder.
    frontier = util.Stack()
    # Agrega el estado inicial a la pila como una tupla (estado, camino) donde estado es el 
    # estado actual y camino es una lista vacía para almacenar las acciones tomadas.
    frontier.push((problem.getStartState(), []))
    explored = set()

    # Mientras la pila no esté vacía:
    # Saca el nodo superior de la pila (usando pop() en la pila) y desempaqueta el 
    # estado y el camino.
    while not frontier.isEmpty():
        state, path = frontier.pop()
        # Verifica si el estado actual es un estado objetivo usando
        # problem.isGoalState(estado). Si lo es, devuelve el camino como una lista de acciones.
        if problem.isGoalState(state):
            return path
        # Si el estado no es un estado objetivo, expande el nodo obteniendo sus sucesores
        # utilizando problem.getSuccessors(estado). Esto te dará una lista de triples 
        # (sucesor, acción, costo)
        if state not in explored:
            explored.add(state)
            successors = problem.getSuccessors(state)
            # Para cada sucesor en la lista de sucesores:
            for successor, action, _ in successors:
                # Verifica si el sucesor no ha sido explorado antes (no está en la lista de nodos
                # explorados). Si es así, agrega el sucesor a la pila junto con el camino
                # actualizado (agregando la acción a la lista de acciones).
                if successor not in explored:
                    new_path = path + [action]
                    # Marca el estado como explorado agregándolo a la lista de nodos explorados.
                    frontier.push((successor, new_path))
    # Si la pila se vacía y no se ha encontrado un estado objetivo, devuelve una lista vacía 
    # para indicar que no se encontró solución.
    return []  # No se encontró solución


def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""
    
    # Algoritmo: Búsqueda en grafo por anchura
    # Utiliza una cola (util.Queue()) para la frontera. Esto significa que el primer nodo agregado a 
    # la frontera será el primero en ser explorado. Es decir, se explora en niveles, primero todos 
    # los nodos a una profundidad dada antes de avanzar a la siguiente profundidad.
    # El resto del códgio es el mismo algoritmo que el de la búsqueda por profundidad. Simplemente
    # cambia el tipo de almacenamiento de datos (pilas --> util.Stack(); colas --> util.Queue())
    """
    frontier = util.Queue()
    frontier.push((problem.getStartState(), []))
    explored = set()

    while not frontier.isEmpty():
        state, path = frontier.pop()

        if problem.isGoalState(state):
            return path

        if state not in explored:
            explored.add(state)
            successors = problem.getSuccessors(state)
            for successor, action, _ in successors:
                if successor not in explored:
                    new_path = path + [action]
                    frontier.push((successor, new_path))

    return []  # No se encontró solución
    """

    frontier = util.Queue()
    frontier.push((problem.getStartState(), []))
    explored = []
    
    while not frontier.isEmpty():
        node, path = frontier.pop()
        if problem.isGoalState(node):
            return path
        if not node in explored:
            explored.append(node)
            for coord, move, cost in problem.getSuccessors(node):
                frontier.push((coord, path + [move]))

    return []  # No se encontró solución

    


def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    
    # Algoritmo: Búsqueda de costo uniforme
    # PriorityQueue permite ordenar los nodos en función de su coste acumulado, 
    # de modo que los nodos con un menor costo acumulado se exploran primero.
    frontier = util.PriorityQueue()
    # Agrega el estado inicial a la cola de prioridad junto con el camino vacío y el coste 
    # acumulado cero.
    # 0: Esto es el costo acumulado hasta el estado actual. Al comenzar desde el estado
    # inicial, el costo acumulado es cero, ya que no hemos realizado ninguna acción.
    # 0 (segundo parámetro de frontier.push()): Esto es la prioridad que se asigna al nodo
    # en la cola de prioridad. En UCS, la prioridad es igual al costo acumulado hasta 
    # el estado actual. Por lo tanto, cuando se agrega el estado inicial, su costo
    # acumulado es cero, y esa es la prioridad inicial.
    frontier.push((problem.getStartState(), [], 0), 0)  # (estado, camino, coste acumulado)
    explored = set()

    while not frontier.isEmpty():
        state, path, total_cost = frontier.pop()

        if problem.isGoalState(state):
            return path

        if state not in explored:
            explored.add(state)
            successors = problem.getSuccessors(state)
            for successor, action, step_cost in successors:
                if successor not in explored:
                    new_path = path + [action]
                    # Calcula el nuevo coste acumulado
                    new_cost = total_cost + step_cost
                    frontier.push((successor, new_path, new_cost), new_cost)

    return []  # No se encontró solución


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """
    Search the node that has the lowest combined cost and heuristic first.
    """
    # Algoritmo: Búsqueda A*
    
    # Crear una cola de prioridad (frontier) para almacenar estados a explorar.
    frontier = util.PriorityQueue()
    # Crear un diccionario "counts" para almacenar el costo acumulado y la heurística para cada estado.
    counts = util.Counter()
    # Inicializar el estado actual con el estado inicial del problema y una lista de acciones vacía.
    current = (problem.getStartState(), [])
    # Calcular la heurística para el estado inicial y almacenarla en "counts".
    counts[str(current[0])] += heuristic(current[0], problem)
    # Agregar el estado inicial a la cola de prioridad con prioridad basada en el valor calculado.
    frontier.push(current, counts[str(current[0])])
    
    # Inicializar una lista "explored" para llevar un registro de los estados explorados.
    explored = []

    while not frontier.isEmpty():
        # Extraer el estado y el camino asociado con la menor prioridad de la cola de prioridad.
        state, path = frontier.pop()

        # Verificar si el estado actual es un estado objetivo.
        if problem.isGoalState(state):
            return path

        # Verificar si el estado ya ha sido explorado previamente.
        if not tuple(state) in explored:
            explored.append(tuple(state))

            # Generar sucesores del estado actual y explorarlos.
            for coord, action, cost in problem.getSuccessors(state):
                new_path = path + [action]
                # Actualizar el costo acumulado y la heurística para el sucesor.
                counts[str(coord)] = problem.getCostOfActions(new_path)
                counts[str(coord)] += heuristic(coord, problem)
                # Agregar el sucesor a la cola de prioridad con su nueva prioridad.
                frontier.push((coord, new_path), counts[str(coord)])

    return []  # No se encontró solución





# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
